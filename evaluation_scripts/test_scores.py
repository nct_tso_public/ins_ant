import numpy as np
import os
from options_eval import parser
import util_eval as util

def print_scores(instruments,scores,header):

	print('\n{}'.format(header))

	for i,s in zip(instruments,scores):

		num_spaces = max([1, len(header) - (len(i) + 5)])
		print('{}:{}{:.2f}'.format(i, ' '*num_spaces, s))

	print('-'*len(header))

	num_spaces = max([1, len(header) - 9])
	print('Mean:{}{:.2f}\n'.format(' '*num_spaces, np.mean(scores)))

opts = parser.parse_args()
horizon = opts.horizon

# load predictions (NUM_INSTRUMENTS x NUM_FRAMES x NUM_SAMPLES) and targets (NUM_INSTRUMENTS x NUM_FRAMES)
prediction, target, _, _ = util.load_samples(opts)
# use sample mean as an estimate for the predictive expectation
# now prediction and target both have shape (NUM_INSTRUMENTS x NUM_FRAMES)
prediction = prediction.mean(axis=2)

wMAE = []
#pMAE = []

for y,t in zip(prediction,target):

	outside_horizon = (t == horizon)
	inside_horizon = (t < horizon) & (t > 0)
	#anticipating = (y > horizon*.1) & (y < horizon*.9)

	wMAE_ins = np.mean([
		np.abs(y[outside_horizon]-t[outside_horizon]).mean(),
		np.abs(y[inside_horizon]-t[inside_horizon]).mean()
	])
	#pMAE_ins = np.abs(y[anticipating]-t[anticipating]).mean()

	wMAE.append(wMAE_ins)
	#pMAE.append(pMAE_ins)

print_scores(instruments=util.instruments,scores=wMAE,header='== wMAE [min.] ==')
#print_scores(instruments=util.instruments,scores=pMAE,header='== pMAE [min.] ==') # pMAE is not used anymore because of undesirable properties in some situations.